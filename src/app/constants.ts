export class AppSettings {
   //public static HOST                       = 'http://localhost:3000/';
   public static HOST                       = 'http://46.101.22.190:3001/';

   public static ADD_USER                   = AppSettings.HOST+'users/signup';
   public static PROFILE_DETAIL				= AppSettings.HOST+'profile/detail';
   
   public static EMAIL_PATTERN = /^[a-z]+[a-z0-9._]+@[a-z]+\.[a-z.]{2,5}$/;
   public static PHONE_PATTERN = /^[0-9]{10,10}$/;
   public static DATE_PATTERN = /^\d{2}\/\d{2}\/\d{4}$/;
}