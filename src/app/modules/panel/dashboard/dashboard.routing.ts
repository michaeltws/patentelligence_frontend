import { Routes, RouterModule }  from '@angular/router';
import { dashboardComponent } from './dashboard.component';
import { ModuleWithProviders } from '@angular/core';

const routes: Routes = [
  { path: '', component:dashboardComponent}
  
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
