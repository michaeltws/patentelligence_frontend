import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { panelService } from './../panel.service';
import { AppSettings } from './../../../constants';
@Component({
  selector: '[panel-main-header]',
  templateUrl: './main-header.component.html',
  styleUrls: ['./main-header.component.css']
})
export class MainHeaderComponent implements OnInit {
	
	public loggedData:any = {};
	

	@Output()
  	sidebarToggle:EventEmitter<string> = new EventEmitter();

	constructor(public _panelService:panelService, private _router:Router){
		//this.loggedData = JSON.parse(localStorage.getItem('userDetail'));
		this.loggedData = this._panelService.getSession();
		console.log(this.loggedData);
		if(typeof this.loggedData["profile_image"] == "undefined"){
			this.loggedData["profile_image"] = "./assets/img/noimage.png";	
		}
	}

	ngOnInit(){

	}

	sidebarChildToggle(){
		this.sidebarToggle.emit('complete');
	}

	logout(){
		localStorage.removeItem('userDetail');
		let route = '/login';
		this._router.navigate([route]);   

	}
}
