import { Injectable, Compiler } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';

@Injectable()

export class panelService{

    public userDetail:any = {};
	constructor(protected http:Http, private _compiler: Compiler){

	}

    public setSession(userObj){
        this.userDetail = userObj;
        localStorage.setItem('userDetail', JSON.stringify(userObj));
    }

    public getSession(){
        return this.userDetail;
    }
	
}