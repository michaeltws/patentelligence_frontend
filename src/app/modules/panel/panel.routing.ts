import { Routes, RouterModule }  from '@angular/router';
import { panelComponent } from './panel.component';
import { ModuleWithProviders } from '@angular/core';

import { ActiveRouteGuard } from './../services/activate-route-guard';
import { DeactiveRouteGuard } from './../services/deactivate-route-guard';

const routes: Routes = [
	{ 
		path: 'dashboard', 
		component:panelComponent, 
		loadChildren: './dashboard/dashboard.module#DashboardModule',
		canActivate: [DeactiveRouteGuard]
	},
	/*
	{ 
		path: 'profile', 
		component:adminComponent, 
		loadChildren: './profile/profile.module#ProfileModule',
		canActivate: [DeactiveRouteGuard]
	},
	*/
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
