import { Component, OnInit } from '@angular/core';
import { Router, RouteConfigLoadStart, RouteConfigLoadEnd } from '@angular/router';
declare var $: any;

@Component({
	selector:'panel-app',
	templateUrl:'./panel.html',
	styleUrls:[
		'./panel.css'
	]
})

export class panelComponent implements OnInit {
	bodyClasses:string = "skin-blue sidebar-mini";
  	body = document.getElementsByTagName('body')[0];
  	public controlBarDisp:boolean = false;
  	public loadingRouteConfig:boolean = false;

  	constructor (private router: Router){

  	}

	ngOnInit() {
		this.router.events.subscribe(event => {
			if (event instanceof RouteConfigLoadStart) {
                this.loadingRouteConfig = true;
            } else if (event instanceof RouteConfigLoadEnd) {
                this.loadingRouteConfig = false;
            }
        });
	}

	sidebarFunc(e){

		if(this.controlBarDisp){
			this.controlBarDisp = false;
		}
		else{
			this.controlBarDisp = true;	
		}
	}	
}