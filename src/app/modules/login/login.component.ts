import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {FormGroup, AbstractControl, FormBuilder, Validators, FormControl} from '@angular/forms';
import { loginService } from './login.service';
import { AppSettings } from './../../constants';
import { panelService } from './../panel/panel.service';
declare var $: any;

@Component({
	selector:'login-app',
	templateUrl:'./login.html',
	styleUrls:[
		'./login.css'
	]
})

export class loginComponent implements OnInit {
	
	public form:FormGroup;
	public email:AbstractControl;
	public loading:boolean = false;
	public password:AbstractControl;
	public rememberMe:AbstractControl;
	public submitted:boolean = false;
	public message:string = "";
	public loggedData:any = {};

	ngOnInit() { 
	
		/*
		$('input').iCheck({
			checkboxClass: "icheckbox_square-blue",
			radioClass: "iradio_square-blue",
			increaseArea: "20%" // optional
		});
		*/
	}

	constructor(fb:FormBuilder, private _loginService:loginService, private _router: Router, private _panelService:panelService) {
		this.form = fb.group({
	      'email': ['', Validators.compose([Validators.required, Validators.pattern(AppSettings.EMAIL_PATTERN)])],
	      'password': ['', Validators.compose([Validators.required, Validators.minLength(8)])],
		  'rememberMe':['']
	    });
	    this.email = this.form.controls['email'];
	    this.password = this.form.controls['password'];
		this.rememberMe = this.form.controls['rememberMe'];
		
	    if(this._router.url == "/login/logout"){
	    	this.logout();
	    }
		
		$('input').iCheck({
			checkboxClass: "icheckbox_square-blue",
			radioClass: "iradio_square-blue",
			increaseArea: "20%" // optional
		});

	    this.loggedData = this._panelService.getSession();
  	}
	
	public changeValue() {
		this.rememberMe = new FormControl(!this.rememberMe.value);
		//console.log(this.rememberMe);
	}

  	public onSubmit(values:Object):void {
		this.submitted = true;
	    if (this.form.valid) {
			this.loading = true;
	      	this._loginService.login(values).subscribe(userData => {
				this.loading = false;
    			if(userData.success == true){
    				if(typeof userData.detail.profile_image != "undefined"){
			            userData.detail.profile_image =  AppSettings.HOST+"uploads/profileImage/"+userData.detail.profile_image;
			        }
    				localStorage.setItem('userDetail', JSON.stringify(userData.detail));
					this._router.navigate(['/panel/dashboard']);
    				
    			}
    			else if(userData.success == false){
					// Invalid credentials
					this.message = userData.msg;
    			}
    			else{
    				//something went wrong
    			}
	    	});
	    }
  	}

  	logout(){
  		localStorage.removeItem('userDetail');
  	}
}