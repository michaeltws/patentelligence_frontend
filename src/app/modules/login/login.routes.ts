import { Routes, RouterModule }  from '@angular/router';
import { loginComponent } from './login.component';
import { ModuleWithProviders } from '@angular/core';

const routes: Routes = [
  { path: '', component: loginComponent},
  { path: 'logout', component: loginComponent},
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
