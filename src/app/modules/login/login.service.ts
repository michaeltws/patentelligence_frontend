import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import 'rxjs/add/operator/map';

import { AppSettings } from './../../constants';
@Injectable()

export class loginService{
	constructor(private http:Http){

	}

	login(obj:Object):any{
		return this.http.post(AppSettings.HOST+'login', obj).map((response: Response) => response.json());
	}
}