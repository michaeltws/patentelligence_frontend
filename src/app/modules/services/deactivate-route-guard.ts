import { CanActivate } from '@angular/router';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { panelService } from './../panel/panel.service';

@Injectable()
export class DeactiveRouteGuard implements CanActivate {

    constructor(private router : Router, public _panelService:panelService) { }
	canActivate() {
		let token = localStorage.getItem('userDetail');
		if(token) {
			let userDetail = JSON.parse(token);
			this._panelService.setSession(JSON.parse(localStorage.getItem('userDetail')));
			return true;
		} 
		else {
			this.router.navigate(['/login']);
		}
	}  
}