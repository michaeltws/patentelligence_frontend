import { Routes, RouterModule }  from '@angular/router';
import { registerComponent } from './register.component';
import { ModuleWithProviders } from '@angular/core';

const routes: Routes = [
  { path: '', component: registerComponent},
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
