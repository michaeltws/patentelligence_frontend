import { Component, ViewContainerRef, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {FormGroup, AbstractControl, FormBuilder, Validators} from '@angular/forms';
import { registerService } from './register.service';
import { AppSettings } from './../../constants';
import { panelService } from './../panel/panel.service';

import { ToastsManager } from 'ng2-toastr/ng2-toastr';

declare var $: any;

@Component({
	selector:'register-app',
	templateUrl:'./register.html'
})

export class registerComponent implements OnInit {
	
	public form:FormGroup;
	public email:AbstractControl;
	public password:AbstractControl;
	public verify_password:AbstractControl;
	public user_type:AbstractControl;
	public full_name:AbstractControl;
	public submitted:boolean = false;
	public message:string = "";
	public loggedData:any = {};

	

	constructor(fb:FormBuilder, private _registerService:registerService, private _router: Router, private _panelService:panelService, public toastr: ToastsManager, vcr: ViewContainerRef) {
		this.toastr.setRootViewContainerRef(vcr);
		this.form = fb.group({
		  'full_name': ['', Validators.compose([Validators.required, Validators.minLength(4)])],	
		  'user_type': ['0', Validators.compose([Validators.required])],	
	      'email': ['', Validators.compose([Validators.required, Validators.pattern(AppSettings.EMAIL_PATTERN)])],
	      'password': ['', Validators.compose([Validators.required, Validators.minLength(8)])],
	      'verify_password': ['', Validators.compose([Validators.required, Validators.minLength(8)])]
	    }, {validator: this.checkIfMatchingPasswords('password', 'verify_password')});
	    this.email = this.form.controls['email'];
	    this.full_name = this.form.controls['full_name'];
	    this.password = this.form.controls['password'];
	    this.verify_password = this.form.controls['verify_password'];
		this.user_type = this.form.controls['user_type'];
		
	    if(this._router.url == "/login/logout"){
	    	this.logout();
	    }
	    this.loggedData = this._panelService.getSession();
  	}

	ngOnInit() { 
		$('input').iCheck({
			checkboxClass: "icheckbox_square-blue",
			radioClass: "iradio_square-blue",
			increaseArea: "20%" // optional
		});
		
		
	}
	
	checkIfMatchingPasswords(passwordKey: string, passwordConfirmationKey: string) {
		return (group: FormGroup) => {
			let passwordInput = group.controls[passwordKey],
			passwordConfirmationInput = group.controls[passwordConfirmationKey];
			if (passwordInput.value !== passwordConfirmationInput.value) {
				return passwordConfirmationInput.setErrors({notEquivalent: true})
			}
			else {
				return passwordConfirmationInput.setErrors(null);
			}
		}
	}

  	public onSubmit(values:Object):void {
		this.submitted = true;
	    if (this.form.valid) {
	      	// your code goes here
			this._registerService.signup(values).subscribe(response => {
    			if(response.success == true){
					this.form.reset();
					this.form.controls['user_type'].setValue('0');
					this.toastr.info('User has been created successfully!', 'Success!');
					
    			}
    			else if(response.success == false){
					// Invalid credentials
					if (response.error.error.errors){
					    for (var errName in response.error.error.errors) {
	                        this.toastr.error(response.error.error.errors[errName].message, 'Error!');
	                    }
	                } else{
					    this.toastr.error(response.error.	error.message, 'Error!');
					}
    			}
    			else{
    				//something went wrong
    			}
	    	});
	    }
  	}

  	logout(){
  		localStorage.removeItem('userDetail');
  	}
}