import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { registerService } from './register.service';

import { ToastModule } from 'ng2-toastr/ng2-toastr';

import { registerComponent } from './register.component';

import { routing } from './register.routes';

@NgModule({
	imports:[
		FormsModule,
		ReactiveFormsModule,
		CommonModule,
		ToastModule.forRoot(),
		routing
	],
	declarations:[
		registerComponent
	],
	providers:[
		registerService
	]
})

export class RegisterModule{}